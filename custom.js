$(document).ready(function(){
    var index = 1,j=0,temp;
    $(document).on("click","#add",function(){
        var input = $("#input_val").val();
        if(input==""){
            alert("dal input dal");
        }
        else{
            addData(input);
            $("#input_val").val("");
        }
    });

    function addData(input){
        if($(".main").val()=="None"){
            $("ul").append(`<li id="${index}" data-text="${input}">${input}<span class='edit'>edit</span><span class='remove'>remove</span></li>`);
            $("select").append(`<option value="${index}">${input}</option>`);
        }
        else{
            if($("select").last().val() == 'None') {
                if($("select").length > 0 ) {
                    temp =  $("select").last().prev();
                } else {
                    temp = $("select").first();
                }                
            } else {
                temp = $("select").last();
            }
            console.log(temp,temp.val())
            temp = temp.val()
            // temp = $("select").val();
            if($("#"+temp).has('ul').length==0){
                
                $("#"+temp).append(`<ul><li id="${index}" data-text="${input}">${$("#input_val").val()}<span class='edit'>edit</span><span class='remove'>remove</span></li></ul>`);
            } else {
                $("#"+temp).find('ul').append(`<li id="${index}" data-text="${input}">${$("#input_val").val()}<span class='edit'>edit</span><span class='remove'>remove</span></li>`);
            }
        }
        $("select").val("None").first().nextAll().remove();
        index++;
    }


    $(document).on("change","select",function(){
        temp = $(this).val();
        $(this).nextAll().remove();
        if($("#"+temp).has('ul').length!=0){
            var a = "<select><option value='None'>None</option>";
            $("#"+temp+">ul>li").each(function(){
                a+=`<option value="${$(this).attr('id')}">${$(this).data('text')}</option>`;
            })
            a+="</select>";
            $(".dropdown").append(a);
        }
    });

    $(document).on("click",".edit",function(){
        $('#input_val').val($(this).closest('li').data('text'));
        $('#add').attr('id','update').text('update').attr('data-id',$(this).closest('li').attr('id'));
    });
    $(document).on("click","#update",function(){
        $('#'+$(this).attr('data-id')).text($('#input_val').val()).attr('data-text',$('#input_val').val()).append("<span class='edit'>edit</span><span class='remove'>remove</span>");
        $('#update').attr('id','add').text('add').attr('data-id','');
    });
    $(document).on("click",".remove", function(){
        console.log($(this).closest('li').index());
        $(this).closest('li').remove();
        $("select").remove($(this).closest('li').index());
    });
});