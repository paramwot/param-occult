# method-1
# if __name__ == '__main__':
#     ans_list = []
#     N = int(input())
#     for lines in range(N):
#         stringInput = str(input())
#         strList = stringInput.split(" ")
#         if strList[0]=='insert':
#             ans_list.insert(int(strList[1]),int(strList[2]))
#         elif strList[0]=='print':
#             print(ans_list)
#         elif strList[0]=='remove':
#             ans_list.remove(int(strList[1])) 
#         elif strList[0]=='append':
#             ans_list.append(int(strList[1]))
#         elif strList[0]=='sort':
#             ans_list.sort()
#         elif strList[0]=='pop':
#             ans_list.pop()
#         elif strList[0]=='reverse':
#             ans_list.reverse() 
# 
# method-2           
# T = int(input().strip())
# L = []
# for t in range(T):
#     args = input().strip().split(" ")
#     if args[0] == "print":
#         print(L)
#     elif len(args) == 3:
#         getattr(L, args[0])(int(args[1]), int(args[2]))
#     elif len(args) == 2:
#         getattr(L, args[0])(int(args[1]))
#     else:
#         getattr(L, args[0])()

#method-3
n = int(input())
l = []
for _ in range(n):
    s = input().split()
    cmd = s[0]
    args = s[1:]
    if cmd !="print":
        cmd += "("+ ",".join(args) +")"
        eval("l."+cmd)
    else:
        print(l)